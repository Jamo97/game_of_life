require 'rspec'

require_relative 'game_of_life.rb'

describe 'Game of life' do

  let!(:world) {World.new}

  context 'World' do
    let!(:subject) {World.new}

    it 'should create a world object' do
      expect(subject.class).to be(World)
    end

    it 'should response to methods' do
      expect(subject).to respond_to(:height)
      expect(subject).to respond_to(:width)
      expect(subject).to respond_to(:grid)
      expect(subject).to respond_to(:cells)
      expect(subject).to respond_to(:living_neighbours)
    end

    it 'should create grid on initialize' do
      expect(subject.grid.class).to be Array

      subject.grid.each do |row|
        expect(row.class).to be Array
      end
    end

    it 'should detect living neighbours' do
      subject.grid[0][0].alive = true
      expect(subject.grid[0][0]).to be_alive
      subject.grid[3][1].alive = true
      expect(subject.grid[3][1]).to be_alive
      subject.grid[6][1].alive = true
      expect(subject.grid[6][1]).to be_alive
      subject.grid[1][3].alive = true
      expect(subject.grid[1][3]).to be_alive
      subject.grid[4][3].alive = true
      expect(subject.grid[4][3]).to be_alive
      subject.grid[2][5].alive = true
      expect(subject.grid[2][5]).to be_alive
      subject.grid[6][6].alive = true
      expect(subject.grid[6][6]).to be_alive

      expect(subject.living_neighbours(subject.grid[0][1]).count).to eq 1
      expect(subject.living_neighbours(subject.grid[4][1]).count).to eq 1
      expect(subject.living_neighbours(subject.grid[6][3]).count).to eq 0
      expect(subject.living_neighbours(subject.grid[1][4]).count).to eq 2
      expect(subject.living_neighbours(subject.grid[3][4]).count).to eq 2
    end
  end

  context 'Cell' do
    let!(:subject) {Cell.new}

    it 'should create a new cell object' do
      expect(subject.class).to be(Cell)
    end

    it 'should response to methods' do
      expect(subject).to respond_to(:y)
      expect(subject).to respond_to(:x)
      expect(subject).to respond_to(:alive)
      expect(subject).to respond_to(:alive?)
      expect(subject).to respond_to(:dead?)
      expect(subject).to respond_to(:die!)
      expect(subject).to respond_to(:live!)
      expect(subject).to respond_to(:wanted)
    end

    it 'should initialitze properly' do
      expect(subject.alive).to be false
      expect(subject.x).to be 0
      expect(subject.y).to be 0
    end
  end

  context 'Game' do
    let!(:subject) {Game.new}

    it 'should create a game object' do
      expect(subject.class).to be(Game)
    end

    it 'should response to methods' do
      expect(subject).to respond_to(:world)
      expect(subject).to respond_to(:seeds)
    end

    it 'should inititialize properly' do
      expect(subject.world.class).to be(World)
      expect(subject.seeds).to be_empty
    end

    it 'should plant seeds properly' do
      seeds_planted = [[1, 2], [0, 2]]
      game = Game.new({world: world, seeds: seeds_planted})

      seeds_planted.each do |seed|
        expect(world.grid[seed[0]][seed[1]]).to be_alive
      end
    end
  end

  context 'Rules' do
    context 'RULE 1: Any live cell with fewer than two live neighbours dies, as if by underpopulation.' do
      it 'should kill a live cell with 1 one live neighbour' do
        seeds_planted = [[0, 1], [0, 2]]
        game = Game.new({world: world, seeds: seeds_planted})

        game.next_round!

        seeds_planted.each do |seed|
          expect(world.grid[seed[0]][seed[1]]).to be_dead
        end
      end
    end


    context 'RULE 2: Any live cell with two or three live neighbours lives on to the next generation.' do
      it 'should kill a live cell with lower than 2 live neighbour' do
        seeds_planted = [[2, 2], [3, 4], [5, 4], [5, 5], [3, 3], [6, 5], [6, 6]]
        game = Game.new({world: world, seeds: seeds_planted})

        game.next_round!

        expect(world.grid[2][2]).to be_dead
        expect(world.grid[3][4]).to be_dead
        expect(world.grid[3][3]).to be_alive
        expect(world.grid[5][4]).to be_alive
        expect(world.grid[5][5]).to be_alive
        expect(world.grid[6][5]).to be_alive
        expect(world.grid[6][6]).to be_alive
      end
    end

    context 'Any live cell with more than three live neighbours dies, as if by overpopulation.' do
      it 'should kill a live cell with 1 one live neighbour' do
        # seeds_planted = [[2, 2], [3, 3], [3, 4], [4, 3], [4, 2], [5, 4]]
        seeds_planted = [[0, 1], [1, 1], [2, 1], [2, 2], [1, 2]]
        game = Game.new({world: world, seeds: seeds_planted})
        #expect(world.living_neighbours(world.grid[1][1]).count).to eq 4

        game.next_round!
        #expect(world.grid[0][1]).to be_alive
        #expect(world.grid[1][1]).to be_dead
        #expect(world.grid[2][1]).to be_alive
        #expect(world.grid[2][2]).to be_alive
        #expect(world.grid[1][2]).to be_dead

        # expect(world.grid[2][2]).to be_dead
        # expect(world.grid[3][3]).to be_alive
        # expect(world.grid[3][4]).to be_alive
        # expect(world.grid[4][3]).to be_dead
        # expect(world.grid[4][2]).to be_alive
        # expect(world.grid[5][4]).to be_dead
      end
    end

    context 'Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.' do
      it 'should kill a live cell with 1 one live neighbour' do
        seeds_planted = [[0, 1], [0, 2]]
        game = Game.new({world: world, seeds: seeds_planted})

        game.next_round!

        seeds_planted.each do |seed|
          expect(world.grid[seed[0]][seed[1]]).to be_dead
        end
      end
    end
  end
end