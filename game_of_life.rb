class Game
  attr_accessor :world
  attr_accessor :seeds

  def initialize(args = {})
    self.world = args[:world] || World.new
    self.seeds = args[:seeds] || []

    bring_to_life(seeds)
  end

  def next_round!

    world.cells.each do |cell|
      living_cells = 0
      living_cells = world.living_neighbours(cell).count if cell.alive?

      # RULE 1
      if cell.alive? && living_cells < 2
        cell.wanted = true
      end

      # RULE 2
      if cell.alive? && [2, 3].include?(living_cells)
        cell.wanted = false
      end

      # RULE 3
      # TODO: kann nicht bis 4 zählen
      if cell.alive? && living_cells > 3
        puts "ANYBODY?"
        puts living_cells
        cell.wanted = true
      end

      # RULE 4

    end
    decide_on_life_and_death
  end

  private

  def bring_to_life(seeds)
    seeds.each do |seed|
      world.grid[seed[0]][seed[1]].alive = true
    end
  end

  def decide_on_life_and_death
    world.cells.each do |cell|
      if cell.wanted
        cell.alive = false
      end
    end
  end

end

class World
  attr_accessor :height
  attr_accessor :width
  attr_accessor :cells
  attr_accessor :kill
  attr_reader :grid

  def initialize(args = {})
    self.height = args[:height] || 7
    self.width = args[:width] || 7
    self.cells = []

    init_field
  end

  # cell = Cell.new
  def living_neighbours(cell)
    neighbours = []
    puts "#{cell.y} - #{cell.x}"
    # Element über cell
    neighbours << check_cols(cell.y - 1, cell.x) unless cell.y.zero?
    # Element auf gleicher row
    neighbours << check_cols(cell.y, cell.x, true)
    # Element unter cell
    neighbours << check_cols(cell.y + 1, cell.x) unless (cell.y + 1) == height
    puts "COUNT"
    p neighbours
    p neighbours.count
    neighbours.reject!(&:empty?)

    neighbours
  end

  private

  def check_cols(row, cell, equal_row = false)
    seeds = []
    living_seeds = []
    seeds << grid[row][cell - 1] unless cell.zero?
    #puts "COL 1"
    #p seeds
    seeds << grid[row][cell] unless equal_row
    #puts "COL 2"
    #p seeds
    seeds << grid[row][cell + 1] unless (cell + 1) == width
    #puts "COL 2"
    #p seeds

    seeds.each do |seed|
      puts "seed" if seed.alive?
      p seed if seed.alive?
      living_seeds << seed if seed.alive?
    end
    puts "LIVING SEEDS"
    puts living_seeds.count
    p living_seeds
    living_seeds
  end

  def init_field
    @grid = []

    (0...height).each do |row|
      rows = []
      (0...width).each do |col|
        cell = Cell.new({y: row, x: col})
        rows << cell
        @cells << cell
      end
      @grid << rows
    end
  end

end

class Cell
  attr_accessor :y
  attr_accessor :x
  attr_accessor :alive
  attr_accessor :wanted

  def initialize(args = {})
    self.alive ||= false
    self.wanted ||= false
    self.y = args[:y] || 0
    self.x = args[:x] || 0
  end

  def die!
    self.alive = false
  end

  def live!
    self.alive = true
  end

  def alive?
    alive
  end

  def dead?
    !alive
  end
end